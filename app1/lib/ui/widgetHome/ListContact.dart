import 'package:app1/controller/contact_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListContact extends GetView<ContactController> {
  final String? titre;
  final String? phone, mail, prenom;
  final int? index;
  const ListContact(
      {Key? key, this.titre, this.phone, this.mail, this.prenom, this.index})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 15),
      width: 800,
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(titre ?? 'Jean-Paul Larue',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18)),
                      const SizedBox(
                        width: 5,
                      ),
                      Text(prenom ?? 'Jean-Paul Larue',
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18)),
                    ],
                  ),
                  PopupMenuButton(
                    onSelected: (value) =>
                        controller.contactList.removeAt(index!),
                    itemBuilder: (context) => [
                      const PopupMenuItem(
                        child: Text("Supprimer"),
                        value: 1,
                      )
                    ],
                  )
                ],
              ),
              Row(
                children: [
                  const Icon(Icons.phone),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    phone ?? ' 06 71 59 57 60',
                    style: const TextStyle(fontSize: 15),
                  ),
                ],
              ),
              Row(
                children: [
                  const Icon(Icons.mail),
                  const SizedBox(
                    width: 5,
                  ),
                  Text(
                    mail ?? ' jp.larue@laposte.fr',
                    style: const TextStyle(fontSize: 15),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
