import 'package:app1/controller/contact_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class Infos extends GetView<ContactController> {
  const Infos({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    String? _nom;
    String? _prenom;
    String? _societe;
    String? _poste;
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        color: Colors.white,
      ),
      width: size.width * 0.8,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Text(
              'Informations',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 20,
            ),
            Row(
              children: [
                Expanded(
                  flex: 1,
                  child: TextFormField(
                    initialValue: controller.selectedContact?.name,
                    onChanged: (nom) {
                      if (controller.selectedContact != null) {
                        controller.selectedContact?.name = nom;
                      } else {
                        controller.createdContact.name = nom;
                      }
                    },
                    validator: (nom) {
                      if (nom == null || nom.isEmpty) {
                        return 'Veuillez entrer un nom';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      labelText: 'Nom',
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                    flex: 1,
                    child: TextFormField(
                      initialValue: controller.selectedContact?.prenom,
                      onChanged: (prenom) {
                        if (controller.selectedContact != null) {
                          controller.selectedContact?.prenom = prenom;
                        } else {
                          controller.createdContact.prenom = prenom;
                        }
                      },
                      decoration: const InputDecoration(
                        labelText: 'Prenom',
                        border: OutlineInputBorder(),
                      ),
                    )),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              onChanged: (societe) => _societe = societe,
              validator: (societe) {
                return null;
              },
              decoration: const InputDecoration(
                labelText: 'Societe',
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              onChanged: (poste) => _poste = poste,
              validator: (poste) {
                return null;
              },
              decoration: const InputDecoration(
                labelText: 'Poste',
                border: OutlineInputBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
