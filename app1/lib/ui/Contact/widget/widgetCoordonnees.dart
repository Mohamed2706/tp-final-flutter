import 'package:app1/controller/contact_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';

class Coordonnees extends GetView<ContactController> {
  const Coordonnees({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    String? _telPers;
    String? _telPro;
    String? _telFixe;
    String? _mail;
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        color: Colors.white,
      ),
      width: size.width * 0.8,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Text(
              'Coordonnees',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              initialValue: controller.selectedContact?.phone,
              onChanged: (telPers) {
                if (controller.selectedContact != null) {
                  controller.selectedContact?.phone = telPers;
                } else {
                  controller.createdContact.phone = telPers;
                }
              },
              validator: (telPers) {
                if (RegExp(r'(^(?:[+0]9)?[0-9]{10,12}$)')
                        .hasMatch(telPers ?? "") !=
                    true) {
                  return 'Veuillez entrer un numéro';
                }
                return null;
              },
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.call),
                labelText: 'Telephone personnel',
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              onChanged: (telPro) => controller.createdContact.mobile = telPro,
              validator: (telPro) {
                return null;
              },
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.phone_iphone),
                labelText: 'Telephone professionnel',
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              onChanged: (telFixe) => _telFixe = telFixe,
              validator: (telFixe) {
                return null;
              },
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.phone_enabled_sharp),
                labelText: 'Telephone fixe',
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            TextFormField(
              initialValue: controller.selectedContact?.email,
              onChanged: (mail) {
                if (controller.selectedContact != null) {
                  controller.selectedContact?.email = mail;
                } else {
                  controller.createdContact.email = mail;
                }
              },
              validator: (mail) {
                if (RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                        .hasMatch(mail ?? "") !=
                    true) {
                  return 'Veuillez entrer un mail uniquement';
                }
                return null;
              },
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.mail),
                labelText: 'Mail',
                border: OutlineInputBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
