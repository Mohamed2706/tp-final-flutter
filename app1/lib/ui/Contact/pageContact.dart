import 'package:app1/controller/contact_controller.dart';
import 'package:app1/model/Contact.dart';
import 'package:app1/ui/Contact/widget/widgetCoordonnees.dart';
import 'package:app1/ui/Contact/widget/widgetInformation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';

class DetailsContact extends GetView<ContactController> {
  const DetailsContact({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _formKey = GlobalKey<FormState>();

    return Scaffold(
      backgroundColor: Colors.blueGrey.shade100,
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade100,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              controller.selectedContact?.name ?? "Creer un contact",
              style: const TextStyle(color: Colors.black),
            ),
          ],
        ),
        actions: [
          InkWell(
            onTap: () {
              if (_formKey.currentState!.validate()) {
                if (controller.selectedContact != null) {
                  controller.updateContact();
                } else {
                  controller.contactList.add(Contact(
                      email: controller.createdContact.email,
                      phone: controller.createdContact.phone,
                      mobile: controller.createdContact.mobile,
                      name: controller.createdContact.name,
                      prenom: controller.createdContact.prenom));
                }
                Get.toNamed("home");
              }
            },
            child: const Icon(
              Icons.add_task_outlined,
              color: Colors.black,
              size: 35,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 50),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Infos(),
                SizedBox(
                  height: 20,
                ),
                Coordonnees(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
