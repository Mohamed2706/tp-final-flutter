import 'package:app1/controller/contact_controller.dart';
import 'package:app1/model/Contact.dart';
import 'package:app1/ui/widgetHome/ListContact.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:get/get_navigation/src/extension_navigation.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';

class Home extends GetView<ContactController> {
  const Home({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        titleTextStyle: const TextStyle(color: Colors.black, fontSize: 25),
        backgroundColor: Colors.blueGrey.shade100,
        centerTitle: true,
        title: const Text("Mon app"),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.grey.shade400,
        child: const Icon(
          Icons.add,
          color: Colors.black38,
        ),
        onPressed: () {
          controller.selectedContact = null;
          Get.toNamed("contact");
        },
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Obx(
          () {
            return Center(
                child: ListView(
              padding: const EdgeInsets.all(10),
              children: List.generate(
                  controller.contactList.length,
                  (index) => Column(
                        children: [
                          InkWell(
                            onTap: () {
                              controller.selectedContact =
                                  controller.contactList[index];
                              controller.index = index;
                              Get.toNamed("contact");
                            },
                            child: ListContact(
                              phone: controller.contactList[index].phone,
                              titre: controller.contactList[index].name,
                              mail: controller.contactList[index].email,
                              prenom: controller.contactList[index].prenom,
                              index: index,
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                        ],
                      )),
            ));
          },
        ),
      ),
    );
  }
}
