import 'package:get/get.dart';

import 'contact_controller.dart';

class ContactBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ContactController>(
      () => ContactController(),
    );
  }
}
