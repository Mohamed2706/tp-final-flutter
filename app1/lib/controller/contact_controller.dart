import 'package:app1/model/Contact.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';

class ContactController {
  RxList<Contact> contactList = [
    Contact(
        email: 'p@j.fr',
        name: "Jean-Paul",
        phone: "0606060606",
        mobile: "0606060606",
        prenom: "Larue"),
  ].obs;

  Contact createdContact =
      Contact(email: "", phone: "", mobile: "", name: "", prenom: "");

  Contact? selectedContact;

  int? index;

  void updateContact() {
    contactList[index!] = selectedContact!;
  }
}
